"""askme URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from app import views

urlpatterns = [
    path('new/', views.index, name = 'index'),
    path('login/', views.login, name = 'login'),
    path('question/<int:qid>/', views.question, name = 'question'),
    path('signup/', views.signup, name = 'signup'),
    path('settings/', views.settings, name = 'settings'),
    path('ask/', views.ask, name = 'ask'),
    path('index_tags/<str:tag>/', views.index_tags, name = 'index_tags'),
    path('admin/', admin.site.urls),
    path('hot/', views.hot, name = 'hot'),
    path('logout/', views.logout_user, name = 'logout'),
    path('teg_add/<int:qid>', views.teg_add, name = 'teg_add'),
    path('like_ajax/<int:qid>/ajax', views.like_ajax, name='like_ajax'),
    path('dislike_ajax/<int:qid>/ajax', views.dislike_ajax, name='dislike_ajax')
]
