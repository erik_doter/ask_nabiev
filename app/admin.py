from django.contrib import admin
from .models import Teg
from .models import Profile
from .models import Question
from .models import Answer
from .models import TegQuestion
from .models import Like
from .models import LikeAnswer

admin.site.register(Teg)
admin.site.register(Profile)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(TegQuestion)
admin.site.register(Like)
admin.site.register(LikeAnswer)