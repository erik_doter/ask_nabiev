from __future__ import unicode_literals

from django.shortcuts import render, redirect, reverse

from django.http import HttpResponse
import random
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .models import Teg
from .models import Question
from .models import Answer
from .models import TegQuestion
from .models import Like
from .models import LikeAnswer
from .models import Profile
from django.core.paginator import EmptyPage, InvalidPage, PageNotAnInteger, Paginator
from .includes import answer_count
from .includes import paginate
from .includes import popular_tegs
from .includes import likes_dislikes
from .includes import likes_dislikes_answer
from .includes import rate_answer
from .includes import rate
from .includes import sort_t
from app import forms
from django.http import JsonResponse


def index(request):
		if request.user.is_authenticated:
			authterized = '1'
		else:
			authterized = '0'
		profiles = Profile.objects.filter(nickname=request.user)
		if not profiles:
			prof = {}
		else:
			for profile in profiles:
				prof = profile
		questions = Question.objects.all().order_by('-add_date')
		question_list = []
		for i in range(len(questions)):
			question_list.append(questions[i])
		question_page, page = paginate(question_list, request, 5)
		teg_question = TegQuestion.objects.all()
		likes = likes_dislikes()[0]
		dislikes = likes_dislikes()[1]
		word = "Новые"
		return render(request, 'index.html', {
				'profile': prof,
				'authterized': authterized,
				'questions': question_page,
				'popular_tegs': popular_tegs(),
				'page' : page,
				'likes': likes,
				'dislikes': dislikes,
				'rates' : rate(likes, dislikes),
				'answers' : answer_count()[1],
				'answer_count' : answer_count()[0],
				'teg_question' : teg_question,
				'word': word,
			})

def hot(request):
	if request.user.is_authenticated:
		authterized = '1'
	else:
		authterized = '0'
	profiles = Profile.objects.filter(nickname=request.user)
	if not profiles:
		prof = {}
	else:
		for profile in profiles:
			prof = profile
	questions = Question.objects.all().order_by('-add_date')
	question_list = []
	for i in range(len(questions)):
		question_list.append(questions[i])
	teg_question = TegQuestion.objects.all()
	likes = likes_dislikes()[0]
	dislikes = likes_dislikes()[1]
	rates = rate(likes, dislikes)
	sort_rate = sorted(rates, key=lambda x: x['rate'])
	question_list_sorted = sort_t(sort_rate, questions)
	question_page, page = paginate(question_list_sorted, request, 5)
	word = "Лучшие"
	return render(request, 'index.html', {
		'authterized': authterized,
		'profile': prof,
		'questions': question_page,
		'page' : page,
		'popular_tegs': popular_tegs(),
		'likes': likes,
		'dislikes': dislikes,
		'rates' : rates,
		'answers' : answer_count()[1],
		'answer_count' : answer_count()[0],
		'teg_question' : teg_question,
		'word': word,
	})


def login(request):
	if request.method == 'GET':
		form = forms.LoginForm()
	else:
		form = forms.LoginForm(data=request.POST)
		if form.is_valid():
			user = auth.authenticate(request, **form.cleaned_data)
			if user is not None:
				auth.login(request, user)
				if 'next' in request.GET:
					redirect_to = request.GET['next']
					return redirect(redirect_to)
				else:
					return redirect('/new')
			else:
				return render(request, 'login.html', {
					'Warning': '1',
					'form': form,
					'popular_tegs': popular_tegs(),
				})
	return render(request, 'login.html', {
		'authterized': '0',
		'Warning': '0',
		'form': form,
		'popular_tegs': popular_tegs(),
	})

def question(request, qid):
	if request.user.is_authenticated:
		authterized = '1'
	else:
		authterized = '0'
	profiles = Profile.objects.filter(nickname=request.user)
	if not profiles:
		prof = {}
	else:
		for profile in profiles:
			prof = profile
	questions = Question.objects.all().order_by('-add_date')
	question_list = []
	for i in range(len(questions)):
		question_list.append(questions[i])
	for q in question_list:
		if q.id == qid:
			question = q
	ans = Answer.objects.filter(questionID = qid)
	teg_question = TegQuestion.objects.filter(questionID = qid)
	likes = Like.objects.filter(questionID = qid)
	count_likes = 0
	count_dislikes = 0
	for like in likes:
		if like.choice == 'L':
			count_likes = count_likes + 1
		else:
			count_dislikes = count_dislikes + 1
	likes = likes_dislikes_answer(ans)[0]
	dislikes = likes_dislikes_answer(ans)[1]
	rates = rate_answer(likes, dislikes, ans)
	answer_list_sorted = sort_t(rates, ans)
	answer_page, page = paginate(answer_list_sorted, request, 5)
	if request.method == 'GET':
		form = forms.AnswerForm(prof, question)
		return render(request, 'question.html', {
				'warning': '0',
				'form': form,
				'authterized': authterized,
				'profile': prof,
				'question': question,
				'popular_tegs': popular_tegs(),
				'page': page,
				'answers': answer_page,
				'likes_count': count_likes,
				'likes_answers': likes,
				'rate': rates,
				'dislikes_answers': dislikes,
				'dislikes_count': count_dislikes,
				'teg_question' : teg_question,
		})
	if request.user.is_authenticated:
		form = forms.AnswerForm(prof, question, data = request.POST)
		if form.is_valid():
			answer = form.save()
			return redirect(reverse('question', kwargs={'qid': qid}))
	form = forms.AnswerForm(prof, question)
	return render(request, 'question.html', {
		'warning': '1',
		'form': form,
		'authterized': authterized,
		'profile': prof,
		'question': question,
		'popular_tegs': popular_tegs(),
		'page': page,
		'answers': answer_page,
		'likes_count': count_likes,
		'likes_answers': likes,
		'rate': rates,
		'dislikes_answers': dislikes,
		'dislikes_count': count_dislikes,
		'teg_question' : teg_question,
	})


def signup(request):
	if request.method == 'GET':
		form = forms.SignupForm()
		return render(request, 'signup.html', {
			'warning': '0',
			'form': form,
			'authterized': '0',
			'popular_tegs': popular_tegs(),
		})
	form = forms.SignupForm(data = request.POST)
	if form.is_valid():
		profile = form.save(commit=False)
		profiles = Profile.objects.filter(nickname=profile.nickname)
		if not profiles:
			profile = form.save()
			user = User.objects.create_user(profile.nickname, profile.mail, profile.password)
			user.save()
			return redirect('/login')
		else:
			return render(request, 'signup.html', {
				'warning': '1',
				'form': form,
				'authterized': '0',
				'popular_tegs': popular_tegs(),
			})
	return render(request, 'signup.html', {
		'warning': '0',
		'form': form,
		'authterized': '0',
		'popular_tegs': popular_tegs(),
	})

def settings(request):
	profiles = Profile.objects.filter(nickname=request.user)
	for profile in profiles:
		prof = profile
	return render(request, 'settings.html', {
		'authterized': '1',
		'profile': prof,
		'popular_tegs': popular_tegs(),
	})

@login_required
def ask(request):
	profiles = Profile.objects.filter(nickname=request.user)
	for profile in profiles:
		prof = profile
	if request.method == 'GET':
		form = forms.QuestionForm(prof)
		return render(request, 'ask.html', {
			'authterized': '1',
			'profile': prof,
			'form': form,
			'popular_tegs': popular_tegs(),
		})
	buttonask = request.POST.get("ask")
	if buttonask == 'ask':
		form = forms.QuestionForm(prof, data = request.POST)
		if form.is_valid():
			question = form.save()
			return redirect(reverse('question', kwargs={'qid': question.id}))
	else:
		form = forms.QuestionForm(prof, data = request.POST)
		if form.is_valid():
			question = form.save()
			return redirect(reverse('teg_add', kwargs={'qid': question.id}))
	return render(request, 'ask.html', {
		'authterized': '1',
		'profile': prof,
		'form': form,
		'popular_tegs': popular_tegs(),
	})

def index_tags(request, tag):
	if request.user.is_authenticated:
		authterized = '1'
	else:
		authterized = '0'
	profiles = Profile.objects.filter(nickname=request.user)
	if not profiles:
		prof = {}
	else:
		for profile in profiles:
			prof = profile
	questions = Question.objects.all().order_by('-add_date')
	question_list = []
	for i in range(len(questions)):
		question_list.append(questions[i])
	tags = tag
	question_page, page = paginate(question_list, request, 5)
	teg_question = TegQuestion.objects.all()
	likes = likes_dislikes()[0]
	dislikes = likes_dislikes()[1]
	return render(request, 'index_tags.html', {
			'authterized': authterized,
			'profile': prof,
			'tag' : tags,
			'popular_tegs': popular_tegs(),
			'questions': question_page,
			'page' : page,
			'answer_count' : answer_count()[0],
			'teg_question' : teg_question,
			'likes': likes,
			'dislikes': dislikes,
			'rates' : rate(likes, dislikes),
		})

@login_required
def logout_user(request):
	auth.logout(request)
	return redirect(reverse('login'))

@login_required
def teg_add(request, qid):
	profiles = Profile.objects.filter(nickname=request.user)
	for profile in profiles:
		prof = profile
	if request.method == 'GET':
		form = forms.TegForm()
		return render(request, 'teg.html', {
			'warning': '0',
			'authterized': '1',
			'profile': prof,
			'form': form,
			'popular_tegs': popular_tegs(),
		})
	buttonask = request.POST.get("ask")
	if buttonask == 'ask':
		form = forms.TegForm(data = request.POST)
		if form.is_valid():
			teg = form.save(commit = False)
			tegs = Teg.objects.filter(title=teg.title)
			for te in tegs:
				t = te
			if not tegs:
				teg = form.save()
			else:
				teg = t
			questions = Question.objects.filter(id = qid)
			for que in questions:
				q = que
			tq = TegQuestion(tegID = teg, questionID = q)
			tegQuestion = TegQuestion.objects.filter(tegID=tq.tegID).filter(questionID=tq.questionID)
			if not tegQuestion:
				tq.save()
			else:
				return render(request, 'teg.html', {
					'warning': '1',
					'authterized': '1',
					'profile': prof,
					'form': form,
					'popular_tegs': popular_tegs(),
				})
			return redirect(reverse('question', kwargs={'qid': qid}))
	else:
		form = forms.TegForm(data = request.POST)
		if form.is_valid():
			teg = form.save(commit = False)
			tegs = Teg.objects.filter(title=teg.title)
			for te in tegs:
				t = te
			if not tegs:
				teg = form.save()
			else:
				teg = t
			questions = Question.objects.filter(id = qid)
			for que in questions:
				q = que
			tq = TegQuestion(tegID = teg, questionID = q)
			tegQuestion = TegQuestion.objects.filter(tegID=tq.tegID).filter(questionID=tq.questionID)
			if not tegQuestion:
				tq.save()
			else:
				return render(request, 'teg.html', {
				'warning': '1',
				'authterized': '1',
				'profile': prof,
				'form': form,
				'popular_tegs': popular_tegs(),
				})
		return redirect(reverse('teg_add', kwargs={'qid': qid}))

	return render(request, 'teg.html', {
		'authterized': '1',
		'profile': prof,
		'form': form,
		'popular_tegs': popular_tegs(),
	})

@login_required
def like_ajax(request, qid):
	profiles = Profile.objects.filter(nickname=request.user)
	for profile in profiles:
		prof = profile
	questions = Question.objects.filter(id=qid)
	for question in questions:
		q = question
	likesf = Like.objects.filter(questionID = qid)
	warning = 0
	for l in likesf:
		if l.profileID.id == prof.id:
			warning = 1
	if warning == 0:
		Like.objects.create(profileID = prof, questionID = q, choice = 'L')
	likes = Like.objects.filter(questionID = qid)
	count_likes = 0

	for like in likes:
		if like.choice == 'L':
			count_likes = count_likes + 1
	return JsonResponse({
		'likes_count': count_likes,
	})

@login_required
def dislike_ajax(request, qid):
	profiles = Profile.objects.filter(nickname=request.user)
	for profile in profiles:
		prof = profile
	questions = Question.objects.filter(id=qid)
	for question in questions:
		q = question
	likesf = Like.objects.filter(questionID = qid)
	warning = 0
	for l in likesf:
		if l.profileID.id == prof.id:
			warning = 1
	if warning == 0:
		Like.objects.create(profileID = prof, questionID = q, choice = 'D')
	likes = Like.objects.filter(questionID = qid)
	count_dislikes = 0
	for like in likes:
		if like.choice == 'D':
			count_dislikes = count_dislikes + 1
	return JsonResponse({
		'dislikes_count': count_dislikes
	})



