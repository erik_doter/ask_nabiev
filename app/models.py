from django.db import models

class Teg(models.Model):
    title = models.CharField(max_length = 64)

    def __str__(self):
        return 'Teg: {}'.format(self.title)

class TegQuestion(models.Model):
    tegID = models.ForeignKey('Teg', on_delete = models.CASCADE)
    questionID = models.ForeignKey('Question', on_delete = models.CASCADE)

    def __str__(self):
        return 'TeqQuestion: {}'.format(self.id)

class Like(models.Model):
    CHOICE_LIKE = [
        ('L', 'Like'),
        ('D', 'Dislike'),
    ]
    profileID = models.ForeignKey('Profile', on_delete = models.CASCADE)
    questionID = models.ForeignKey('Question', on_delete = models.CASCADE)
    choice = models.CharField(max_length = 3, choices = CHOICE_LIKE)

    def __str__(self):
        return 'Like: {}'.format(self.id)

class LikeAnswer(models.Model):
    CHOICE_LIKE = [
        ('L', 'Like'),
        ('D', 'Dislike'),
    ]
    profileID = models.ForeignKey('Profile', on_delete = models.CASCADE)
    answerID = models.ForeignKey('Answer', on_delete = models.CASCADE)
    choice = models.CharField(max_length = 3, choices = CHOICE_LIKE)

    def __str__(self):
        return 'Like: {}'.format(self.id)

class Profile(models.Model):
    nickname = models.CharField(max_length = 64)
    mail = models.CharField(max_length = 64)
    avatar = models.ImageField(default='64x64.jpg')
    password = models.CharField(max_length = 64)
    registration_date = models.DateTimeField(auto_now_add = True, null = True)

    def __str__(self):
        return 'Profile: {}'.format(self.nickname)

class Question(models.Model):
    profileID = models.ForeignKey('Profile', on_delete = models.CASCADE)
    title = models.CharField(max_length = 64)
    body = models.TextField(blank = True, null = True)
    add_date = models.DateTimeField(auto_now_add = True, null = True)

    def __str__(self):
        return 'Question: {}'.format(self.title)

class Answer(models.Model):
    questionID = models.ForeignKey('Question', on_delete = models.CASCADE)
    profileID = models.ForeignKey('Profile', on_delete = models.CASCADE)
    body = models.TextField(blank = True, null = True)
    add_date = models.DateTimeField(auto_now_add = True, null = True)
    correct = models.BooleanField(default = 0)

    def __str__(self):
        return 'Answer: {}'.format(self.id)
