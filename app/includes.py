from __future__ import unicode_literals

from django.shortcuts import render

from django.http import HttpResponse
import random
from .models import Teg
from .models import Question
from .models import Answer
from .models import TegQuestion
from .models import Like
from .models import LikeAnswer
from .models import Profile
from django.core.paginator import EmptyPage, InvalidPage, PageNotAnInteger, Paginator

def answer_count():
    questions = Question.objects.all().order_by('-add_date')
    question_list = []
    for i in range(len(questions)):
        question_list.append(questions[i])
    answer_count_list = []
    ans = Answer.objects.all()
    for question in question_list:
        d = {'id': question.id, 'count': 0}
        answer_count_list.append(d)
        for answer in ans:
            if question.id == answer.questionID.id:
                i = 0
                for a in answer_count_list:
                    if a['id'] == question.id:
                        key = i
                    i = i + 1
                answer_count_list[key]['count'] = answer_count_list[key]['count'] + 1
    return answer_count_list, ans

def paginate(objects_list, request, per_page):
    questions = Question.objects.all().order_by('-add_date')
    question_list = []
    for i in range(len(questions)):
        question_list.append(questions[i])
    page_number = request.GET.get('page')
    if (page_number == None):
        page_number = 1

    paginator = Paginator(objects_list, per_page)
    if (paginator.num_pages == 0):
        return None, None

    try:
        page = paginator.page(page_number)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    except InvalidPage:
        page = paginator.page(1)

    return page.object_list, page

def popular_tegs():
    questions = Question.objects.all().order_by('-add_date')
    question_list = []
    for i in range(len(questions)):
        question_list.append(questions[i])
    teg_question = TegQuestion.objects.all()
    teg = Teg.objects.all()
    popular = []
    for t in teg:
        d = {'name': t.title, 'count': 0}
        popular.append(d)
    for t_q in teg_question:
        for p in popular:
            if t_q.tegID.title == p['name']:
                i = 0
                for pop in popular:
                    if p['name'] == pop['name']:
                        k = i
                        popular[k]['count'] = popular[k]['count'] + 1
                    i = i + 1
    popular_sort = sorted(popular, key=lambda x: x['count'])
    popular_sort.reverse()
    best_5 = []
    for i in range(5):
        best_5.append(popular_sort[i])
    return best_5

def likes_dislikes():
    questions = Question.objects.all().order_by('-add_date')
    question_list = []
    for i in range(len(questions)):
        question_list.append(questions[i])
    likes = Like.objects.all()
    like_count_list = []
    dislike_count_list = []
    for q in questions:
        l = {'id': q.id, 'count': 0}
        m = {'id': q.id, 'count': 0}
        like_count_list.append(l)
        dislike_count_list.append(m)
        i = 0
        for like in like_count_list:
            if like['id'] == q.id:
                lkey = i
            i = i + 1
        i = 0
        for dislike in dislike_count_list:
            if dislike['id'] == q.id:
                dkey = i
            i = i + 1
        for like in likes:
            if q.id == like.questionID.id:
                if like.choice == 'L' :
                    like_count_list[lkey]['count'] = like_count_list[lkey]['count'] + 1
                else :
                    if like.choice=='D':
                        dislike_count_list[dkey]['count'] = dislike_count_list[dkey]['count'] + 1
    return like_count_list, dislike_count_list

def likes_dislikes_answer(answers):
    questions = Question.objects.all().order_by('-add_date')
    question_list = []
    for i in range(len(questions)):
        question_list.append(questions[i])
    likes_answer_list = []
    dislikes_answer_list = []
    for a in answers:
        l = {'id': a.id, 'count': LikeAnswer.objects.filter(answerID = a.id).filter(choice = 'L').count()}
        d = {'id': a.id, 'count': LikeAnswer.objects.filter(answerID = a.id).filter(choice = 'D').count()}
        likes_answer_list.append(l)
        dislikes_answer_list.append(d)
    return likes_answer_list, dislikes_answer_list

def rate_answer(likes, dislikes, answers):
    questions = Question.objects.all().order_by('-add_date')
    question_list = []
    for i in range(len(questions)):
        question_list.append(questions[i])
    rate_answer_list = []
    for a in answers:
        i = 0
        for l in likes:
            if l['id'] == a.id:
                key = i
            i = i + 1
        i = 0
        for d in dislikes:
            if d['id'] == a.id:
                dkey = i
            i = i + 1
        r = {'id': a.id, 'rate': likes[key]['count'] - dislikes[dkey]['count']}
        rate_answer_list.append(r)
    return rate_answer_list

def rate(likes, dislikes):
    questions = Question.objects.all().order_by('-add_date')
    question_list = []
    for i in range(len(questions)):
        question_list.append(questions[i])
    rate_list = []
    for que in question_list:
        d = {'id': que.id, 'rate': 0}
        rate_list.append(d)
        i = 0
        for r in rate_list:
            if r['id'] == que.id:
                key = i
            i = i + 1
        rate_list[key]['rate'] = likes[key]['count'] - dislikes[key]['count']
    return rate_list

def sort_t(rate, a_q):
    list = []
    for sr in rate:
        for q in a_q:
            if sr['id'] == q.id:
                i = 0
                for que in a_q:
                    if que.id == q.id:
                        k = i
                        list.append(a_q[k])
                    i = i + 1
    list.reverse()
    return list