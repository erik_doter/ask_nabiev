# Generated by Django 3.0.4 on 2020-04-24 15:19

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0015_like'),
    ]

    operations = [
        migrations.AddField(
            model_name='like',
            name='choice',
            field=models.CharField(choices=[('L', 'Like'), ('D', 'Dislike')], default=django.utils.timezone.now, max_length=3),
            preserve_default=False,
        ),
    ]
