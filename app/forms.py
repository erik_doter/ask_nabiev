from django import forms
from app.models import Question
from app.models import Answer
from app.models import Profile, Teg

class LoginForm(forms.Form):
    username = forms.CharField(max_length = 64)
    password = forms.CharField(widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data['username']
        if ' ' in username:
            raise forms.ValidationError('Space!!!')
        else:
            return username

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['title', 'body']
    def __init__(self, profile, *args, **kwargs):
        self.pofileID = profile
        super().__init__(*args, **kwargs)
    def save(self, commit=True):
        question = Question(**self.cleaned_data)
        question.profileID = self.pofileID
        if commit:
            question.save()
        return question
    def clean_body(self):
        body = self.cleaned_data['body']
        if len(body) == 0:
            raise forms.ValidationError('Empty')
        else:
            return body

class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['body']
    def __init__(self, profile, question, *args, **kwargs):
        self.pofileID = profile
        self.questionID = question
        super().__init__(*args, **kwargs)
    def save(self, commit=True):
        answer = Answer(**self.cleaned_data)
        answer.profileID = self.pofileID
        answer.questionID = self.questionID
        if commit:
            answer.save()
        return answer

    def clean_body(self):
        body = self.cleaned_data['body']
        if len(body) == 0:
            raise forms.ValidationError('Empty')
        else:
            return body

class SignupForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['nickname', 'mail', 'avatar', 'password']
        widgets = {
            'password': forms.PasswordInput,
        }

    def clean_nickname(self):
        nickname = self.cleaned_data['nickname']
        if ' ' in nickname:
            raise forms.ValidationError('Space!!!')
        else:
            return nickname

class TegForm(forms.ModelForm):
    class Meta:
        model = Teg
        fields = ['title']

